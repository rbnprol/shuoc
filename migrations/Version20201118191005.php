<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201118191005 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE purchase ADD CONSTRAINT FK_6117D13BD55BB00C FOREIGN KEY (delivery_province_id) REFERENCES province (id)');
        $this->addSql('CREATE INDEX IDX_6117D13BD55BB00C ON purchase (delivery_province_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE purchase DROP FOREIGN KEY FK_6117D13BD55BB00C');
        $this->addSql('DROP INDEX IDX_6117D13BD55BB00C ON purchase');
    }
}
