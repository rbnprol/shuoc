<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201117142326 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE purchase_product ADD CONSTRAINT FK_C890CED4BE9E54E2 FOREIGN KEY (state_purchase_product_id) REFERENCES status_purchase_product (id)');
        $this->addSql('CREATE INDEX IDX_C890CED4BE9E54E2 ON purchase_product (state_purchase_product_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE purchase_product DROP FOREIGN KEY FK_C890CED4BE9E54E2');
        $this->addSql('DROP INDEX IDX_C890CED4BE9E54E2 ON purchase_product');
    }
}
