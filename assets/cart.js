
window.deleteProductCart = function (product) {

    $.ajax({
        url: $urlDeleteProductCart,
        method: 'POST',
        data: {
            product: product
        },
        beforeSend: function () {
            $overlay.show();
        },
        success: function (response) {
            $overlay.hide();
            toastr[response.status](response.message);
            $("#cart").load(location.href + " #cart");
            $("#cartLength").load(location.href + " #cartLength");
            $("#cartLengthMobile").load(location.href + " #cartLengthMobile");
        }
    })
};

window.sumProductCart = function (id, isSum) {

    let value = $("#" + id),
        oldNumber = value.val();

    console.log(isSum);
    if(isSum) {
        value.val(parseInt(oldNumber) + 1);
    }else{
        value.val(parseInt(oldNumber) - 1);
    }

    $.ajax({
        url: $urlAddProductCart,
        method: 'POST',
        data: {
            product: id,
            quantity: value.val(),
            isSum: isSum
        },
        beforeSend: function () {
            $overlay.show();
        },
        success: function (response) {
            toastr[response.status](response.message);
            $("#cart").load(location.href + " #cart");
            $("#cartLength").load(location.href + " #cartLength");
            $("#cartLengthMobile").load(location.href + " #cartLengthMobile");
            $overlay.hide();
        }
    })
};

