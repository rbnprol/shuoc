/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import 'bootstrap/dist/css/bootstrap.min.css';
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-table/dist/bootstrap-table.min.css';
import 'bootstrap-select/dist/css/bootstrap-select.min.css';
import 'toastr/build/toastr.min.css';
import './styles/app.css';

// Need jQuery? Install it with "yarn add jquery", then uncomment to import it.
import $ from 'jquery';
import 'popper.js/dist/popper.min';
import 'bootstrap/dist/js/bootstrap.min';
import 'bootstrap-table/dist/bootstrap-table.min';
import 'bootstrap-table/dist/bootstrap-table-locale-all.min';
import 'bootstrap-select/dist/js/bootstrap-select.min';
import toastr from 'toastr'
import moment from 'moment';

/**
 * Global variables
 */
global.$ = global.jQuery = $;
global.toastr = toastr;
global.moment = moment;
global.$overlay = $('.cover_overlay');
global.$modalLogout = $('#modalLogout');
global.$modalForm = $('#modalCenterForm');

$.fn.bootstrapTable.defaults.classes = 'table table-condensed table-hover table-no-bordered bg-white';
$.fn.bootstrapTable.defaults.sidePagination = 'server';
$.fn.bootstrapTable.defaults.method = 'POST';
$.fn.bootstrapTable.defaults.contentType = 'application/x-www-form-urlencoded';
$.fn.bootstrapTable.defaults.pagination = true;
$.fn.bootstrapTable.defaults.smartDisplay = true;
$.fn.bootstrapTable.defaults.pageList = [25, 50, 100, 500, 1000];
$.fn.bootstrapTable.defaults.pageSize = 25;
$.fn.bootstrapTable.defaults.exportTypes = ['csv', 'txt', 'excel'];
$.fn.bootstrapTable.defaults.reorderableColumns = true;
$.fn.bootstrapTable.defaults.cookiesEnabled = ['bs.table.sortOrder', 'bs.table.sortName', 'bs.table.pageList', 'bs.table.columns', 'bs.table.searchText', 'bs.table.filterControl'];
$.fn.bootstrapTable.defaults.showColumns = true;
$.fn.bootstrapTable.defaults.showRefresh = true;
$.fn.bootstrapTable.defaults.showToggle = false;
$.fn.bootstrapTable.defaults.showExport = true;
$.fn.bootstrapTable.defaults.smartDisplay = true;
$.fn.bootstrapTable.defaults.buttonsClass = 'default';
$.fn.bootstrapTable.defaults.iconSize = 'sm';
$.fn.bootstrapTable.defaults.locale = 'es_ES';

toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": true,
    "progressBar": false,
    "positionClass": "toast-top-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
};

$('.carousel').carousel({
    interval: 10000
});

window.scrollToId = function (id) {
    $("html, body").animate({scrollTop: $('#' + id).offset().top}, 500);
};

window.resetFormFilters = function (idForm) {
    let $form = $('#' + idForm);
    $form.trigger("reset");
    $form.find('.selectpicker').each(function () {
        $(this).selectpicker('deselectAll');
        $(this).selectpicker('render');
        $(this).selectpicker('refresh');
        $(this).selectpicker('render');
    });
    $form.find('.chosen-select').each(function () {
        $(this).val('').trigger("chosen:updated");
    });

    $form.find('.js-switch').each(function () {
        let idCkbox = $(this).attr('id');
        $('#' + idCkbox).prop('checked', false);
    });

    $form.submit();
};

window.showModalForm = function (title, content, url, idForm, table = null, reloadDivs = null, reloadUrl = false) {

    $modalForm.find('#submit').unbind();
    $modalForm.modal('show');
    $modalForm.find('.modal-body').html(content);
    $modalForm.find('.modal-title').html(title);
    $('.selectpicker').selectpicker('refresh');

    $modalForm.find("#submit").bind('click', function () {
            $.ajax({
                method: 'post',
                url: url,
                data: new FormData($('#' + idForm)[0]),
                contentType: false,
                processData: false,
                cache: false,
                beforeSend: function () {
                    $overlay.show();
                    $modalForm.modal('hide');
                },
                success: function (res) {
                    $overlay.hide();
                    if ('string' == typeof res) {
                        $modalForm(title, res, url, idForm);
                    } else {
                        $modalForm.modal('hide');
                        toastr[res.status](res.message);
                        if (reloadUrl) {
                            setTimeout(
                                window.location.reload(), 1000);
                        } else {
                            if (table) {
                                table.bootstrapTable('refresh');
                            }
                            if (reloadDivs) {
                                $.each(reloadDivs, function (k, v) {
                                    $('#' + v).load(location.href + ' #' + v);
                                });
                            }
                        }
                    }
                }
            });
            $(this).unbind();
        }
    )
    ;
};

window.initStandardTable = function ($table, $columns, $url, $filter) {

    $table.bootstrapTable({
        toolbar: '#toolbar-table',
        url: $url,
        columns: $columns,
        responseHandler: responseHandler,
        queryParams: queryParams
    });

    $table.on('check.bs.table uncheck.bs.table ' +
        'check-all.bs.table uncheck-all.bs.table', function () {
        $selections = getIdSelections($table);
    });

    $table.on('refresh.bs.table', function () {
        $selections = [];
    });

    function getIdSelections() {
        return $.map($table.bootstrapTable('getSelections'), function (row) {
            return row.master_id;
        });
    }

    function queryParams(params) {
        $table.bootstrapTable('showLoading');
        let data = {
            limit: params.limit,
            offset: params.offset,
            order: params.order
        };
        if (params.sort) {
            data.sort = params.sort.replace("_", ".");
        }
        return $filter.serialize() + '&' + $.param(data);
    }

    function responseHandler(res) {
        $table.bootstrapTable('hideLoading');
        $.each(res.rows, function (i, row) {
            row.state = $.inArray(row.master_id, $selections) !== -1;
        });
        return res;
    }

    $filter.submit(function (event) {
        event.preventDefault();
        $table.bootstrapTable('getData').length <= 0 ? $table.bootstrapTable('refresh') : $table.bootstrapTable('selectPage', 1);
        scrollToId('toolbar-table');
        $selections = [];
    });
};

window.showModalLogout = function () {
    $modalLogout.find('#logout').unbind();
    $modalLogout.modal('show');

    $modalLogout.find("#logout").bind('click', function () {
        $overlay.show();
        $modalLogout.modal('hide');
        $(this).unbind();
    });
};

window.dateFormatter = function(value, row) {
    if(value){
        return moment(value.date).format('DD/MM/YYYY');
    }
    return '-';
};

window.timeFormatter = function(value, row) {
    if(value){
        return moment(value.date).format('HH:mm:ss');
    }
    return '-';
};
