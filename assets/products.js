import './styles/products.css';

$('.carousel .carousel-item').each(function () {
    let minPerSlide = 3;
    let next = $(this).next();
    if (!next.length) {
        next = $(this).siblings(':first');
    }
    next.children(':first-child').clone().appendTo($(this));

    for (let i = 0; i < minPerSlide; i++) {
        next = next.next();
        if (!next.length) {
            next = $(this).siblings(':first');
        }

        next.children(':first-child').clone().appendTo($(this));
    }
});

window.sumProduct = function (idProduct, stock) {
    let id = $("#" + idProduct),
        oldNumber = id.val();
    if (parseInt(oldNumber) >= stock) {
        return;
    }
    id.val(parseInt(oldNumber) + 1);
};

window.subtractProduct = function (idProduct) {
    let id = $("#" + idProduct),
        oldNumber = id.val();
    if (parseInt(oldNumber) <= 1) {
        return;
    }
    id.val(parseInt(oldNumber) - 1);
};

window.addCart = function (url, id) {

    $.ajax({
        url: url,
        method: 'POST',
        data: {
            quantity: $("#" + id).val()
        },
        beforeSend: function () {
            $overlay.show();
        },
        success: function (response) {
            $("#" + id).val(1);
            $("#cartLength").load(location.href + " #cartLength");
            $("#cartLengthMobile").load(location.href + " #cartLengthMobile");
            $overlay.hide();
            toastr[response.status](response.message);
        }
    })
};
