initStandardTable($table, $tableColumns, $tableUrl, $filter);

window.actionsPurchasesFormatter = function (value, row) {

    let btn_changeState = `<li class="list-item-sm"><button class="btn btn-light btn-sm" title="Cambiar estado" onClick="changeStatus(` + row.master_id + `)"><i class="fas fa-toggle-on fa-sm"></i></button></li>`;

    return `
        <div class="btn-group"> 
            <button class="dropdown-toggle btn btn-default btn-sm" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-cog"></i></button>
            <ul class="dropdown-menu">
                <li>
                    <ul class="nav nav-pills"> 
                        ` + btn_changeState + `
                    </ul>
                </li>
            </ul>
        </div>
    `;
};

window.imageFormatter = function (value, row) {
    return `<img src="` + $urlUploads + `/products/` + row.product_id + `/` + value + `" width="50" alt="` + row.product_name + `">`;
};

window.statusPurchaseProductFormatter = function (value, row) {

    let span = '';

    switch (value) {
        case 1:
            span = '<span class="badge badge-secondary">PENDIENTE</span>';
            break;
        case 2:
            span = '<span class="badge badge-primary">ENVIADO</span>';
            break;
        case 3:
            span = '<span class="badge badge-success">ENTREGADO</span>';
            break;
        case 4:
            span = '<span class="badge badge-danger">DEVOLUCIÓN</span>';
            break;
        case 5:
            span = '<span class="badge badge-warning">RECIBIDO</span>';
            break;
        default:
            span = '<span class="badge badge-info">' + value + '</span>';
    }
    return span;
};

window.moneyFormatter = function (value, row) {
    return value + '€';
};

window.amountFormatter = function (value, row) {
    let total = value * row.master_quantity;
    let iva = (total * 21) / 100;
    return Number(total + iva).toFixed(2) + '€';
};

window.changeStatus = function (id) {
    let url = $urlChangeStatus.replace(':id', id),
        title = 'Cambio de estado';

    $.ajax({
        method: 'post',
        url: url,
        data: [],
        beforeSend: function () {
            $overlay.show();
        },
        success: function (data) {
            $overlay.hide();
            showModalForm(title, data, url, 'form_changeStatus', $table);
        }
    });
};
