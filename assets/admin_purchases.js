initStandardTable($table, $tableColumns, $tableUrl, $filter);

$table.on('dbl-click-row.bs.table', function (element, row) {
    viewPurchase(row.master_id);
});

window.actionsPurchasesFormatter = function (value, row) {

    let btn_view = `<li class="list-item-sm"><button class="btn btn-light btn-sm" title="Ver Detalle Pedido" onClick="viewPurchase(` + row.master_id + `)"><i class="fas fa-eye fa-sm"></i></button></li>`;
    return `
        <div class="btn-group"> 
            <button class="dropdown-toggle btn btn-default btn-sm" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-cog"></i></button>
            <ul class="dropdown-menu">
                <li>
                    <ul class="nav nav-pills"> 
                        ` + btn_view + `
                    </ul>
                </li>
            </ul>
        </div>
    `;
};

window.viewPurchase = function (id) {
    $overlay.show();
    let url = $urlPurchase.replace(':id', id);
    location.replace(url);
};

window.purchaseLinkFormatter = function (value, row) {
    return value;
};

window.statusFormatter = function (value, row) {
    let percent = (value / row.totalProducts) * 100;

    if (percent === 100) {
        return '<span class="badge badge-success">COMPLETADO</span>';
    }
    if(percent === 0){
        return '<span class="badge badge-danger">' + Math.round( percent ) + '%</span>';
    }
    return '<span class="badge badge-warning">' + Math.round( percent ) + '%</span>';
};

window.clientFormatter = function (value, row){
    if(!value){
        return 'No registrado';
    }
    return value;
}
