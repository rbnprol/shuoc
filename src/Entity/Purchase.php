<?php

namespace App\Entity;

use App\Repository\PurchaseRepository;
use DateTime;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PurchaseRepository::class)
 */
class Purchase
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime", options={"default"="CURRENT_TIMESTAMP"})
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity=Client::class, inversedBy="purchases")
     */
    private $client;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PurchaseProduct", mappedBy="purchase", cascade={"persist"})
     */
    private $purchaseProducts;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $deliveryAddress;

    /**
     * @ORM\ManyToOne(targetEntity=Province::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $deliveryProvince;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $deliveryCity;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $deliveryPostalCode;

    /**
     * @ORM\Column(type="string", length=15)
     */
    private $deliveryPhoneNumber;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $deliveryContactPerson;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $deliveryEmail;

    public function __toString(): string
    {
        return $this->getId();
    }

    public function __construct(Client $client = null)
    {
        if($client){
            $this->setClient($client);
        }

        $this->setCreatedAt(new DateTime('now'));
        $this->purchaseProducts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedAt(): ?DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getClient(): ?Client
    {
        return $this->client;
    }

    public function setClient(?Client $client): self
    {
        $this->client = $client;

        return $this;
    }

    /**
     * @return Collection|PurchaseProduct[]
     */
    public function getPurchaseProducts(): Collection
    {
        return $this->purchaseProducts;
    }

    public function addPurchaseProduct(PurchaseProduct $purchaseProduct): self
    {
        if (!$this->purchaseProducts->contains($purchaseProduct)) {
            $this->purchaseProducts[] = $purchaseProduct;
            $purchaseProduct->setPurchase($this);
        }

        return $this;
    }

    public function removePurchaseProduct(PurchaseProduct $purchaseProduct): self
    {
        if ($this->purchaseProducts->contains($purchaseProduct)) {
            $this->purchaseProducts->removeElement($purchaseProduct);
            // set the owning side to null (unless already changed)
            if ($purchaseProduct->getPurchase() === $this) {
                $purchaseProduct->setPurchase(null);
            }
        }

        return $this;
    }

    public function getDeliveryAddress(): ?string
    {
        return $this->deliveryAddress;
    }

    public function setDeliveryAddress(string $deliveryAddress): self
    {
        $this->deliveryAddress = $deliveryAddress;

        return $this;
    }

    public function getDeliveryProvince(): ?Province
    {
        return $this->deliveryProvince;
    }

    public function setDeliveryProvince(?Province $deliveryProvince): self
    {
        $this->deliveryProvince = $deliveryProvince;

        return $this;
    }

    public function getDeliveryCity(): ?string
    {
        return $this->deliveryCity;
    }

    public function setDeliveryCity(string $deliveryCity): self
    {
        $this->deliveryCity = $deliveryCity;

        return $this;
    }

    public function getDeliveryPostalCode(): ?string
    {
        return $this->deliveryPostalCode;
    }

    public function setDeliveryPostalCode(string $deliveryPostalCode): self
    {
        $this->deliveryPostalCode = $deliveryPostalCode;

        return $this;
    }

    public function getDeliveryPhoneNumber(): ?string
    {
        return $this->deliveryPhoneNumber;
    }

    public function setDeliveryPhoneNumber(string $deliveryPhoneNumber): self
    {
        $this->deliveryPhoneNumber = $deliveryPhoneNumber;

        return $this;
    }

    public function getDeliveryContactPerson(): ?string
    {
        return $this->deliveryContactPerson;
    }

    public function setDeliveryContactPerson(string $deliveryContactPerson): self
    {
        $this->deliveryContactPerson = $deliveryContactPerson;

        return $this;
    }

    public function getDeliveryEmail(): ?string
    {
        return $this->deliveryEmail;
    }

    public function setDeliveryEmail(string $deliveryEmail): self
    {
        $this->deliveryEmail = $deliveryEmail;

        return $this;
    }

}
