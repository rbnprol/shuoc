<?php

namespace App\Repository;

use App\Entity\Purchase;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @method Purchase|null find($id, $lockMode = null, $lockVersion = null)
 * @method Purchase|null findOneBy(array $criteria, array $orderBy = null)
 * @method Purchase[]    findAll()
 * @method Purchase[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PurchaseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Purchase::class);
    }

    public function findDataTable(Request $request): JsonResponse
    {
        $em = $this->getEntityManager();
        $params = $request->request->get('filter_purchase');
        $limit = $request->request->get('limit');
        $offset = $request->request->get('offset');
        $order = $request->request->get('order');
        $sort = $request->request->get('sort');

        $dql =
            "SELECT 
                 master.id AS master_id,
                 master.createdAt AS master_createdAt,
                 client.id AS client_id,
                 SUM(purchaseProduct.quantity) AS totalProducts,
                 SUM(CASE WHEN status.id IN (3,5) THEN purchaseProduct.quantity ELSE 0 END) AS totalStatus
            FROM App:Purchase master
                LEFT JOIN master.client client
                JOIN master.purchaseProducts purchaseProduct
                JOIN purchaseProduct.product product
                JOIN purchaseProduct.statusPurchaseProduct status
            ";

        $dql_w = '';
        $and = ' WHERE ';

        if (@($params['id'] != null)) {
            $dql_w .= $and . " master.id IN (" . $params['id'] . ")";
            $and = ' AND ';
        }

        if (@($params['client'] != null)) {
            $dql_w .= $and . " client.id IN (" . $params['client'] . ")";
            $and = ' AND ';
        }

        $dql_w .= " GROUP BY master.id, master.createdAt, client.name";


        if ($sort) {
            $dql_w .= " ORDER BY {$sort} {$order} ";
        }

        $query = $em->createQuery($dql . $dql_w);
        $count = count($query->getResult());

        if ($limit > 0) {
            $query->setMaxResults($limit);
        }
        $query->setFirstResult($offset);

        $result = $query->getResult();

        return new JsonResponse(array('total' => $count, 'rows' => $result));
    }
}
