<?php

namespace App\Repository;

use App\Entity\StatusPurchaseProduct;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method StatusPurchaseProduct|null find($id, $lockMode = null, $lockVersion = null)
 * @method StatusPurchaseProduct|null findOneBy(array $criteria, array $orderBy = null)
 * @method StatusPurchaseProduct[]    findAll()
 * @method StatusPurchaseProduct[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StatusPurchaseProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StatusPurchaseProduct::class);
    }
}
