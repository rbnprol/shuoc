<?php

namespace App\Repository;

use App\Entity\Purchase;
use App\Entity\PurchaseProduct;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @method PurchaseProduct|null find($id, $lockMode = null, $lockVersion = null)
 * @method PurchaseProduct|null findOneBy(array $criteria, array $orderBy = null)
 * @method PurchaseProduct[]    findAll()
 * @method PurchaseProduct[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PurchaseProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PurchaseProduct::class);
    }

    public function findAllProductsByPurchase()
    {
        return $this->getEntityManager()->createQuery("SELECT SUM(pp.quantity) as totalPurchaseProducts FROM App:PurchaseProduct pp")->getSingleScalarResult();
    }

    public function findDataTable(Request $request, Purchase $purchase): JsonResponse
    {
        $em = $this->getEntityManager();
        $params = $request->request->get('filter_purchase_product');
        $limit = $request->request->get('limit');
        $offset = $request->request->get('offset');
        $order = $request->request->get('order');
        $sort = $request->request->get('sort');

        $dql =
            "SELECT 
                 master.id AS master_id,
                 master.quantity AS master_quantity,
                 product.id AS product_id,
                 product.name AS product_name,
                 product.price AS product_price,
                 product.stock AS product_stock,
                 product.imageUrl AS product_imageUrl,
                 category.name AS category_name,
                 status.id AS status_id
            FROM App:PurchaseProduct master
                JOIN master.statusPurchaseProduct status
                JOIN master.product product
                JOIN product.category category
            WHERE master.purchase =" . $purchase->getId() . "
            ";

        $dql_w = '';
        $and = ' AND ';

        if (@($params['id'] != null)) {
            $dql_w .= $and . " master.id IN (" . $params['id'] . ")";
            $and = ' AND ';
        }

        if (@($params['product'] != null)) {
            $dql_w .= $and . " product.id IN (" . implode(',', $params['product']) . ")";
            $and = ' AND ';
        }

        if (@($params['category'] != null)) {
            $dql_w .= $and . " category.id IN (" . implode(',', $params['category']) . ")";
            $and = ' AND ';
        }

        if ($sort) {
            $dql_w .= " ORDER BY {$sort} {$order} ";
        }

        $query = $em->createQuery($dql . $dql_w);
        $count = count($query->getResult());

        if ($limit > 0) {
            $query->setMaxResults($limit);
        }
        $query->setFirstResult($offset);

        $result = $query->getResult();

        return new JsonResponse(array('total' => $count, 'rows' => $result));
    }
}
