<?php

namespace App\Controller;

use App\Entity\Category;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CategoriesController
 * @package App\Controller
 * @Route("/categories")
 */
class CategoriesController extends AbstractController
{
    /**
     * @Route("/", name="categories")
     */
    public function index(): Response
    {
        $em = $this->getDoctrine()->getManager();

        $categories = $em->getRepository(Category::class)->findBy([
            'isActive' => true
        ]);

        return $this->render('categories/index.html.twig', [
            'categories' => $categories
        ]);
    }

    /**
     * @Route("/category/{category}", name="categories_category")
     * @param Category $category
     * @return Response
     */
    public function category(Category $category): Response
    {
        return $this->render('categories/category.html.twig', [
            'category' => $category
        ]);
    }
}
