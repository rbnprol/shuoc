<?php

namespace App\Controller;

use App\Entity\Client;
use App\Entity\Product;
use App\Entity\Purchase;
use App\Entity\PurchaseProduct;
use App\Entity\StatusPurchaseProduct;
use App\Form\PurchaseType;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/purchase")
 */
class PurchaseController extends AbstractController
{
    /**
     * @Route("/", name="purchase")
     * @param Request $request
     * @return Response
     */
    public function index(Request $request): Response
    {
        $session = $request->getSession();
        /** @var Purchase $purchase */
        $cartPurchase =$session->get('purchase');
        $status = '';
        $message = '';
        $toastr = false;

        if(!$cartPurchase){
            return $this->redirectToRoute('products');
        }

        /** @var Client $client */
        $client = $this->getUser() ?: null;
        $em = $this->getDoctrine()->getManager();

        $purchase = new Purchase($client);

        $form = $this->createForm(PurchaseType::class, $purchase, [
            'client' => $client
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            /** @var StatusPurchaseProduct $statusPurchaseProduct */
            $statusPurchaseProduct = $em->getRepository(StatusPurchaseProduct::class)->find(1);

            /** @var PurchaseProduct $item */
            foreach ($cartPurchase->getPurchaseProducts() as $item) {
                $purchaseProduct = new PurchaseProduct();
                /** @var Product $sessionProduct */
                $sessionProduct = $item->getProduct();
                $product = $em->getRepository(Product::class)->find($sessionProduct->getId());
                $purchaseProduct->setProduct($product);
                $purchaseProduct->setQuantity($item->getQuantity());
                $purchaseProduct->setStatusPurchaseProduct($statusPurchaseProduct);
                $purchase->addPurchaseProduct($purchaseProduct);

                $em->persist($purchase);
            }

            try {
                $em->flush();
                $session->remove('purchase');
                $session->remove('total');
                $toastr = true;
                $status = 'success';
                $message = 'Compra realizada con éxito...';

            } catch (Exception $exception){
                $toastr = true;
                $status = 'error';
                $message = 'Ups... algo ha ido mal';
            }
        }

        return $this->render('purchase/index.html.twig', [
            'form' => $form->createView(),
            'status' => $status,
            'message' => $message,
            'toastr' => $toastr
        ]);
    }
}
