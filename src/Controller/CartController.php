<?php

namespace App\Controller;

use App\Entity\Product;
use App\Entity\Purchase;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CartController
 * @package App\Controller
 * @Route("/cart")
 */
class CartController extends AbstractController
{
    /**
     * @Route("/", name="cart")
     * @return Response
     */
    public function index(): Response
    {
        return $this->render('cart/index.html.twig');
    }

    /**
     * @Route("/add-product", name="cart_add_product")
     * @param Request $request
     * @return JsonResponse
     */
    public function addProductCart(Request $request): JsonResponse
    {
        $session = $request->getSession();
        /** @var Purchase $purchase */
        $purchase = $session->get('purchase');
        $total = (int)$session->get('total');
        $id = (int)$request->request->get('product');
        $isSum = $request->request->get('isSum') === 'true';
        $quantity = (int)$request->request->get('quantity');

        $total = $isSum ? $total + 1 : $total - 1;

        foreach ($purchase->getPurchaseProducts() as $purchaseProduct) {
            /** @var Product $product */
            $product = $purchaseProduct->getProduct();
            if ($product->getId() === $id) {
                $purchaseProduct->setQuantity($quantity);
                break;
            }
        }

        $session->set('total', $total);
        $session->set('purchase', $purchase);

        return new JsonResponse([
            'status' => 'info',
            'message' => "Acción realizada con éxito..."
        ]);
    }

    /**
     * @Route("/delete-product", name="cart_delete_product")
     * @param Request $request
     * @return JsonResponse
     */
    public function deleteProductCart(Request $request): JsonResponse
    {
        $session = $request->getSession();
        /** @var Purchase $purchase */
        $purchase = $session->get('purchase');
        $total = (int)$session->get('total');
        $id = (int)$request->request->get('product');
        $value = 0;

        foreach ($purchase->getPurchaseProducts() as $purchaseProduct) {
            /** @var Product $itemProduct */
            $itemProduct = $purchaseProduct->getProduct();
            if ($itemProduct->getId() === $id) {
                $value = $purchaseProduct->getQuantity();
                $purchase->removePurchaseProduct($purchaseProduct);
            }
        }

        $total -= $value;
        $session->set('total', $total);
        $session->set('purchase', $purchase);

        return new JsonResponse([
            'status' => 'info',
            'message' => "Acción realizada con éxito..."
        ]);
    }
}
