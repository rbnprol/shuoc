<?php

namespace App\Controller;

use App\Entity\Client;
use App\Entity\Purchase;
use App\Form\ChangePasswordType;
use App\Form\Model\ChangePassword;
use App\Form\ProfileType;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/account")
 */
class AccountController extends AbstractController
{
    /**
     * @Route("/", name="account")
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return Response
     */
    public function index(Request $request, UserPasswordEncoderInterface $passwordEncoder): Response
    {
        $em = $this->getDoctrine()->getManager();
        $status = '';
        $message = '';
        $toastr = false;
        $tab = 'purchases';

        /** @var Client $client */
        $client = $this->getUser();

        $purchases = $em->getRepository(Purchase::class)->findBy([
            'client' => $client->getId()
        ],[
            'createdAt' => 'DESC'
        ]);

        $changePassword = new ChangePassword();
        $formChangePass = $this->createForm(ChangePasswordType::class, $changePassword);
        $formProfile = $this->createForm(ProfileType::class, $client);
        $formProfile->handleRequest($request);
        $formChangePass->handleRequest($request);

        if($formProfile->isSubmitted() && $formProfile->isValid()) {
            $em->persist($client);
            $tab = 'profile';
            try {
                $em->flush();
                $toastr = true;
                $status = 'success';
                $message = 'Perfil modificado con éxito';

            } catch (Exception $e) {
                $toastr = true;
                $status = 'error';
                $message = 'Ups... algo ha ido mal';
            }
        }

        if ($formChangePass->isSubmitted() && $formChangePass->isValid()) {
            $client->setPassword($passwordEncoder->encodePassword($client, $changePassword->newPassword));
            $em->persist($client);
            $tab = 'settings';
            try {
                $em->flush();
                $toastr = true;
                $status = 'success';
                $message = 'Contraseña modificada con éxito';

            } catch (Exception $e) {
                $toastr = true;
                $status = 'error';
                $message = 'Ups... algo ha ido mal';
            }
        }

        return $this->render('account/index.html.twig', [
            'tab' => $tab,
            'toastr' => $toastr,
            'status' => $status,
            'message' => $message,
            'purchases' => $purchases,
            'formProfile' => $formProfile->createView(),
            'formChangePass' => $formChangePass->createView()
        ]);
    }
}
