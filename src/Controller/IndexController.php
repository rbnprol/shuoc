<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/")
 */
class IndexController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index(): Response
    {
        $em = $this->getDoctrine()->getManager();

        $products = $em->getRepository(Product::class)->findBy([
            'isActive' => true
        ],[
            'createdAt' => 'DESC'
        ], 100);
        $categories = $em->getRepository(Category::class)->findBy([
            'isActive'=> true
        ]);

        shuffle($products);

        return $this->render('index/index.html.twig', [
            'products' => $products,
            'categories' => $categories
        ]);
    }
}
