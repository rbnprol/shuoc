<?php

namespace App\Controller;

use App\Entity\Product;
use App\Entity\Purchase;
use App\Entity\PurchaseProduct;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ProductsController
 * @package App\Controller
 * @Route("/products")
 */
class ProductsController extends AbstractController
{
    /**
     * @Route("/", name="products")
     */
    public function index(): Response
    {
        $em = $this->getDoctrine()->getManager();

        $products = $em->getRepository(Product::class)->findBy([
            'isActive' => true
        ]);

        return $this->render('products/index.html.twig', [
            'products' => $products,
            'titleProducts' => 'Todos los productos'
        ]);
    }

    /**
     * @Route("/product/{product}", name="products_product")
     * @param Product $product
     * @return Response
     */
    public function productAction(Product $product): Response
    {
        return $this->render('products/product/index.html.twig', [
            'product' => $product
        ]);
    }

    /**
     * @Route("/add-cart/{product}", name="products_add_cart")
     * @param Request $request
     * @param Product $product
     * @return JsonResponse
     */
    public function addCartProduct(Request $request, Product $product): JsonResponse
    {
        $session = $request->getSession();
        $purchase = $session->get('purchase') ?: new Purchase() ;
        $total = (int)$session->get('total');
        $quantity = (int)$request->request->get('quantity');
        $exist = false;

        /** @var PurchaseProduct $purchaseProduct */
        foreach ($purchase->getPurchaseProducts() as $purchaseProduct) {
            /** @var Product $itemProduct */
            $itemProduct = $purchaseProduct->getProduct();
           if($itemProduct->getId() === $product->getId()){
               $purchaseProduct->setQuantity($purchaseProduct->getQuantity() + $quantity);
               $exist = true;
                break;
           }
        }

        if(!$exist){
            $newPurchaseProduct = new PurchaseProduct();
            $newPurchaseProduct->setProduct($product);
            $newPurchaseProduct->setQuantity($quantity);
            $purchase->addPurchaseProduct($newPurchaseProduct);
        }

        $total+=$quantity;

        $session->set('purchase', $purchase);
        $session->set('total', $total);

        return new JsonResponse([
            'status' => 'info',
            'message' => "{$quantity} x {$product->getName()} añadido/s a la cesta."
        ]);
    }
}
