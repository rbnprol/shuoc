<?php

namespace App\Controller\Admin;

use App\Entity\Client;
use App\Entity\Purchase;
use App\Entity\PurchaseProduct;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/")
 */
class AdminController extends AbstractController
{
    /**
     * @Route("/", name="admin")
     */
    public function index()
    {
        $em = $this->getDoctrine()->getManager();

        $totalPurchases = $em->getRepository(Purchase::class)->count([]);
        $totalCustomers = $em->getRepository(Client::class)->count([]);
        $totalPurchaseProducts = $em->getRepository(PurchaseProduct::class)->findAllProductsByPurchase();
        $totalReturned = $em->getRepository(PurchaseProduct::class)->count([
            'statusPurchaseProduct' => 4
        ]);

        return $this->render('admin/dashboard/index.html.twig', [
            'totalPurchases' => $totalPurchases,
            'totalCustomers' => $totalCustomers,
            'totalPurchaseProduct' => $totalPurchaseProducts,
            'totalReturned' => $totalReturned
        ]);
    }
}
