<?php

namespace App\Controller\Admin;

use App\Entity\Purchase;
use App\Entity\PurchaseProduct;
use App\Form\ChangeStatusPurchaseProductType;
use App\Form\Filter\FilterPurchaseProductType;
use App\Form\Filter\FilterPurchaseType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PurchasesController
 * @package App\Controller\Admin
 * @Route("/purchases")
 */
class PurchasesController extends AbstractController
{
    /**
     * @Route("/", name="admin_purchases")
     */
    public function index(): Response
    {
        $filter = $this->createForm(FilterPurchaseType::class);

        return $this->render('admin/purchases/index.html.twig', [
            'filter' => $filter->createView(),
        ]);
    }

    /**
     * @Route("/purchase/{purchase}", name="admin_purchases_purchase")
     * @param Purchase $purchase
     * @return Response
     */
    public function viewPurchase(Purchase $purchase): Response
    {
        $filter = $this->createForm(FilterPurchaseProductType::class);

        return $this->render('admin/purchases/purchase.html.twig', [
            'purchase' => $purchase,
            'filter' => $filter->createView()
        ]);
    }

    /**
     * @Route("/change-status/{purchaseProduct}", name="admin_purchases_change_status")
     * @param Request $request
     * @param PurchaseProduct $purchaseProduct
     * @return JsonResponse|Response
     */
    public function changeStatusPurchaseProduct(Request $request, PurchaseProduct $purchaseProduct)
    {
        $form = $this->createForm(ChangeStatusPurchaseProductType::class, $purchaseProduct);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($purchaseProduct);

            try {
                $em->flush();
                $status = 'success';
                $message = 'Estado modificado con éxito.';
            } catch (\Exception $e) {
                $status = 'error';
                $message = 'Ups... algo ha ido mal';
            }
            return new JsonResponse([
                'status' => $status,
                'message' => $message
            ]);

        }

        return $this->render('admin/purchases/modal/change_status.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/table-find-all-purchases", name="admin_purchases_table_find_all_purchases")
     * @param Request $request
     * @return JsonResponse
     */
    public function tableFindAllPurchases(Request $request): JsonResponse
    {
        $em = $this->getDoctrine()->getManager();

        return $em->getRepository(Purchase::class)->findDataTable($request);
    }

    /**
     * @Route("/table-find-all-purchase_products/{purchase}", name="admin_purchases_table_find_all_purchase_products")
     * @param Request $request
     * @param Purchase $purchase
     * @return JsonResponse
     */
    public function tableFindAllPurchaseProducts(Request $request, Purchase $purchase): JsonResponse
    {
        $em = $this->getDoctrine()->getManager();

        return $em->getRepository(PurchaseProduct::class)->findDataTable($request, $purchase);
    }
}
