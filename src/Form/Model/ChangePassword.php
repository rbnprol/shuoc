<?php


namespace App\Form\Model;

use Symfony\Component\Security\Core\Validator\Constraints as SecurityAssert;


class ChangePassword
{

    /**
     * @SecurityAssert\UserPassword(
     *     message = "changePassword.validator.wrong"
     * )
     */
    public $oldPassword;

    public $newPassword;

}
