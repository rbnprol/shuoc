<?php

namespace App\Form;

use App\Entity\Client;
use App\Entity\Province;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class ProfileType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Nombre',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Este campo no puede estar vacío.'
                    ])
                ]
            ])
            ->add('surnames', TextType::class, [
                'label' => 'Apellidos',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Este campo no puede estar vacío.'
                    ])
                ]
            ])
            ->add('province', EntityType::class, [
                'label' => 'Provincia',
                'placeholder' => 'Selecciona...',
                'class' => Province::class,
                'attr' => [
                    'class' => 'selectpicker',
                    'data-size' => 10,
                    'data-container' => 'body',
                    'data-live-search' => true
                ]
            ])
            ->add('city', TextType::class, [
                'label' => 'Población'
            ])
            ->add('postalCode', TextType::class, [
                'label' => 'Código Postal'
            ])
            ->add('address', TextType::class, [
                'label' => 'Dirección predeterminada'
            ])
            ->add('phoneNumber', NumberType::class, [
                'label' => 'Teléfono'
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Client::class,
        ]);
    }
}
