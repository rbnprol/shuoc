<?php

namespace App\Form\Filter;

use App\Entity\Category;
use App\Entity\Product;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FilterPurchaseProductType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('id', TextType::class, [
                'label' => 'ID Linea',
                'help' => "Puedes concatenar id's separados por comas..."
            ])
            ->add('product', EntityType::class, [
                'label' => 'Producto',
                'class' => Product::class,
                'multiple' => true,
                'attr' => [
                    'class' => 'selectpicker',
                    'data-container' => 'body',
                    'data-size' => 10,
                    'data-live-search' => true,
                    'data-actions-box' => true
                ]
            ])
            ->add('category', EntityType::class, [
                'label' => 'Categoría',
                'class' => Category::class,
                'multiple' => true,
                'attr' => [
                    'class' => 'selectpicker',
                    'data-container' => 'body',
                    'data-size' => 10,
                    'data-live-search' => true,
                    'data-actions-box' => true
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([]);
    }
}
