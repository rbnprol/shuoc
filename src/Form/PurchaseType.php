<?php

namespace App\Form;

use App\Entity\Client;
use App\Entity\Province;
use App\Entity\Purchase;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class PurchaseType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var Client $client */
        $client = $options['client'];

        $builder
            ->add('deliveryProvince', EntityType::class, [
                'label' => 'Provincia *',
                'class' => Province::class,
                'placeholder' => 'Selecciona...',
                'data' => $client ? $client->getProvince() : null,
                'attr' => [
                    'class' => 'selectpicker',
                    'data-size' => 10,
                    'data-container' => 'body',
                    'data-live-search' => true
                ],
                'constraints' => [
                    new NotBlank([
                        'message' => 'Este campo no puede estar vacío.'
                    ])
                ]
            ])
            ->add('deliveryCity', TextType::class, [
                'label' => 'Población *',
                'data' => $client ? $client->getCity() : null,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Este campo no puede estar vacío.'
                    ])
                ]
            ])
            ->add('deliveryPostalCode', TextType::class, [
                'label' => 'Código postal *',
                'data' => $client ? $client->getPostalCode() : null,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Este campo no puede estar vacío.'
                    ])
                ]
            ])
            ->add('deliveryAddress', TextType::class, [
                'label' => 'Dirección de entrega',
                'data' => $client ? $client->getAddress() : null,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Este campo no puede estar vacío.'
                    ])
                ]
            ])
            ->add('deliveryContactPerson', TextType::class, [
                'label' => 'Persona de contacto *',
                'data' => $client ?: null,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Este campo no puede estar vacío.'
                    ])
                ]
            ])
            ->add('deliveryPhoneNumber', TextType::class, [
                'label' => 'Teléfono de contacto *',
                'data' => $client ? $client->getPhoneNumber() : null,
                'required' => true,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Este campo no puede estar vacío.'
                    ]),
                ]
            ])
            ->add('deliveryEmail', TextType::class, [
                'label' => 'Email de contacto *',
                'data' => $client ? $client->getEmail() : null,
                'required' => true,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Este campo no puede estar vacío.'
                    ]),
                ]
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Purchase::class,
            'client' => null
        ]);
    }
}
